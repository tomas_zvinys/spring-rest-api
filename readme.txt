This is a small Springboot + RabbitMQ + MySQL api example:
- Application SpringRestApiApplication.java emulates a server behaviour:
    - it accepts http get / put / post requests on port #8081 and provides replies
    - persists Order data in the MySQL server

- The MySQL server can be accessed on localhost via port #3307 (use Workbench / something similar for graphical interface)
    - Root password: rootPassword

- The application utilises RabbitMQ message broker:
    - the broker has a direct EXCHANGE that routs messages to MSG_QUEUE by using ROUTING_KEY
    - the MSG_QUEUE sorts requests by priority (high-priority-first, low-priority-last), that is defined as an attribute of a Order
    - priority limits are [0 .. 3] inclusively (thus do not expect priority = 5 get higher than priority = 3 - they will be equal)
    - the message broker has an admin interface (management console) exposed on port #15673
        - the console can be accessed via browser (http://localhost:15673/)
        - Username: guest
        - Password: guest

To launch the example using docker:
    - clone the project from Gitlab
    - open commandline and change directory (cd) into the spring-rest-api folder
    - execute Docker command:
        docker-compose -f docker-compose_dev.yml up
        the command will download missing images (if any) and start running the containers (service):
            - MySQL server
            - RabbitMQ
            - Springboot application

Once the Docker containers are running you can start sending requests to the Springboot application.


IMPORTANT:  The RabitMQ is only applied for POST request to add (persist) a new order into the system
    The easiest way to test it - run the test.py Python file - it concurrently sends multiple requests to the application
    Do not expect it to sort all of the messages:
        - the first message is never sorted (gets fetched immediately)
        - the other messages are sorted asynchronously (so that's alto not ideal)


IMPORTANT:  mind the http Content-Type header
            in most cases 'application/xml' is expected, in some cases 'application/json' could work

NOTE:  OrderConsumer utilises Thread.sleep(200); to simulate a slow service

IMPORTANT:  OrderConsumer utilises Thread.sleep(200); to simulate a slow service


Example utilising RabbitMQ priority queue:
POST:
http://localhost:8081/queueApi/addOrder
<order>
    <username>"user9"</username>
    <priority>3</priority>
    <discount>3.05</discount>
</order>


Here are some examples of a simple CRUD API:
POST:
http://localhost:8081/api/addOrders
<list>
    <order>
        <username>"user3"</username>
        <priority>1</priority>
        <discount>2.05</discount>
    </order>
    <order>
        <username>"user3"</username>
        <priority>2</priority>
        <discount>3.05</discount>
    </order>
</list>


GET:
http://localhost:8081/api/order/user1


PUT:
http://localhost:8081/api/update
<order>
    <username>"username1"</username>
    <priority>1</priority>
    <discount>99.0</discount>
    <orderStatus>0</orderStatus>
    <id>1</id>
</order>


DELETE:
localhost:8081/api/delete/1



