package com.tomas.zvinys.service;

import com.tomas.zvinys.exceptions.ResourceNotFoundException;
import com.tomas.zvinys.models.entities.Order;
import com.tomas.zvinys.repository.OrderRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest	// Tell Spring to create application context
class OrderServiceTest {
    private static final Order orderSample1 = new Order(1, "username1", 1, 5.00);
    private static final Order orderSample2 = new Order(2, "username2", 2, 4.00);
    private static final List<Order> orders = new ArrayList<>(){{
        add(orderSample1);
        add(orderSample2);
    }};

    @Autowired    // Let Spring perform dependency injection
    private OrderService service;

    @MockBean    // Do not use real real repository - use a dummy (mock) one, to prevent querying the real one
    private OrderRepository repository;

    private Order cloneOrder(Order oldOrder){
        return new Order(oldOrder.getId(), oldOrder.getUsername(), oldOrder.getPriority(), oldOrder.getDiscount());
    }

    private List<Order> cloneOrderList(List<Order> oldOrders){
        List<Order> orders = new ArrayList<Order>();
        oldOrders.forEach(order->{{orders.add(cloneOrder(order));}});
        return orders;
    }

    /** Create (POST) methods **/

    @Test
    @DisplayName("Should return the order saved")
    void saveOrderTest() {
        when(repository.save(orderSample1)).thenReturn(orderSample1);
        assertEquals(orderSample1, service.saveOrder(orderSample1));
    }

    @Test
    @DisplayName("Should return the orders saved")
    void saveOrdersTest() {
        when(repository.saveAll(orders)).thenReturn(orders);
        assertEquals(orders, service.saveOrders(orders));
    }

    /** Retrieve (GET) methods **/

    @Test
    @DisplayName("Should find an order with id=1")
    void getOrderByIdTest(){
        when(repository.findById(1))
                .thenReturn(java.util.Optional.of(orderSample1));
        assertEquals(1, service.getOrderById(orderSample1.getId()).getId());
    }

    @Test
    @DisplayName("Should find 2 orders for the same username")
    void getOrderByUsername_Ok() {
        String name = orderSample1.getUsername();
        when(repository.findByUsername(name))
                .thenReturn(Optional.of(new ArrayList<Order>() {{
                    add(orderSample1);
                    add(new Order(2, name, 2, 4.00));
                }}));
        assertEquals("username1", service.getOrdersByUsername(name).get(0).getUsername());
        assertEquals("username1", service.getOrdersByUsername(name).get(1).getUsername());
        assertEquals(2, service.getOrdersByUsername(name).size());
    }

    @Test
    @DisplayName("Should throw exception because no orders with such username exist")
    void getOrderByUsername_Nok() {
        String name = orderSample1.getUsername();
        when(repository.findByUsername(name)).thenReturn(Optional.empty());
        assertThatThrownBy(() -> service.updateOrder(orderSample1))
                .isInstanceOf(ResourceNotFoundException.class);
    }

    @Test
    @DisplayName("Should return same orders it has saved")
    void getOrders() {
        when(repository.saveAll(orders)).thenReturn(orders);
        assertEquals(orders, service.saveOrders(orders));
    }

    @Test
    @DisplayName("Should return updated version of the old order")
    void updateOrder_Ok() {
        Order updatedOrder = new Order(1, "username3", 3, 33.33);
        final Order oldOrder = cloneOrder(orderSample1);

        when(repository.findById(1)).thenReturn(java.util.Optional.of(orderSample1));
        when(repository.save(updatedOrder)).thenReturn(updatedOrder);
        assertEquals(updatedOrder, service.updateOrder(updatedOrder));
        System.out.println(service.updateOrder(updatedOrder));
        System.out.println(oldOrder);
        assertNotEquals(oldOrder, service.updateOrder(updatedOrder));
    }

    @Test
    @DisplayName("Should throw exception because order with such id does not exist")
    void updateOrder_Nok() {
        when(repository.findById(anyInt())).thenReturn(Optional.empty());
        assertThatThrownBy(() -> service.updateOrder(orderSample1))
                .isInstanceOf(ResourceNotFoundException.class);
    }

    @Test
    @DisplayName("Should throw exception because order with such id does not exist")
    void deleteOrder_Ok() {
        when(repository.findById(anyInt())).thenReturn(Optional.empty());
        assertThatThrownBy(() -> service.deleteOrder(1))
                .isInstanceOf(ResourceNotFoundException.class);
    }
}