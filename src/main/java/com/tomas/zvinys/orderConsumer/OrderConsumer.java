package com.tomas.zvinys.orderConsumer;

import com.tomas.zvinys.SpringRestApiApplication;
import com.tomas.zvinys.models.UnconfirmedOrder;
import com.tomas.zvinys.models.entities.Order;
import com.tomas.zvinys.service.OrderService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrderConsumer {
    /** Consumer (=Receiver) is a POJO that defines a method for receiving messages **/

    @Autowired
    private OrderService service;

    @RabbitListener(queues = SpringRestApiApplication.MSG_QUEUE)
    public Order consumeMessageFromQueue(UnconfirmedOrder unconfirmedOrder) {
        System.out.println("Message received from queue : " + unconfirmedOrder);
        // Simulate a slow service
        try {
            Thread.sleep(200);
        } catch (Exception e){
            e.printStackTrace();
        }
        Order confirmedOrder = service.confirmOrder(unconfirmedOrder);
        System.out.println("Order confirmed: " + confirmedOrder);
        return confirmedOrder;
    }

}
