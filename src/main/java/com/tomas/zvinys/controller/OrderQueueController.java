package com.tomas.zvinys.controller;

import com.tomas.zvinys.SpringRestApiApplication;
import com.tomas.zvinys.models.UnconfirmedOrder;
import com.tomas.zvinys.models.entities.Order;
import com.tomas.zvinys.exceptions.ResourceNotFoundException;
import com.tomas.zvinys.service.OrderService;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/queueApi")
public class OrderQueueController {
    /** Things that are routed via RabbitMQ **/

    // The order request needs to be published to the RabbitMQ using RabbitTemplate
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @PostMapping(path = "/addOrder", consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public Order bookOrder(@RequestBody UnconfirmedOrder unconfirmedOrder){
        // Modify order status: order is received
        unconfirmedOrder.setOrderStatus(Order.ORDER_RECEIVED);
        System.out.println("New unconfirmedOrder received : " + unconfirmedOrder);

        //Convert unconfirmedOrder to an Amqp Message, modify the message, and send it to the RabbitMQ EXCHANGE with ROUTING_KEY
        //The Exchange will route the unconfirmedOrder to correct request queue (based on RoutingKey)
        //The request queue will order itself by the priority and will prefetch message(s) to the listener
        Object someObj = rabbitTemplate.convertSendAndReceive(
                SpringRestApiApplication.EXCHANGE,
                SpringRestApiApplication.ROUTING_KEY,
                unconfirmedOrder,
                message -> {
                    message.getMessageProperties().setPriority(unconfirmedOrder.getPriority());
                    System.out.println(message.getMessageProperties());
                    message.getMessageProperties().setReplyTo(SpringRestApiApplication.REPLY_MSG_QUEUE);
                    return message;
                }
            );

        //Inform the client that the order has been added to the orderQueue
        return (Order) someObj;
    }
}
