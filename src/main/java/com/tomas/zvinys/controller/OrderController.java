package com.tomas.zvinys.controller;

import com.tomas.zvinys.models.entities.Order;
import com.tomas.zvinys.exceptions.ResourceNotFoundException;
import com.tomas.zvinys.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class OrderController {
    /** Simple end-point stuff (bypasses RabbitMQ queue) **/

    @Autowired
    private OrderService service;

    @GetMapping(path = "/helloWorld", produces = MediaType.TEXT_PLAIN_VALUE)
    @ResponseBody
    public ResponseEntity<String> helloWorld() {
        System.out.println("SpringWorld says Hello");
        return new ResponseEntity<String>("SpringWorld says Hello", HttpStatus.OK);
    }

    @PostMapping(path = "/addOrder", consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public Order addOrder(@RequestBody Order order){
        return service.saveOrder(order);
    }

    // respond with xml or json (depending on the request.Header.Content-Type.value)
    // if Content-Type is not provided with the request - it will return xml
    @PostMapping(path = "/addOrders",
            consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE}, //Both are by default
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE}
    )
    public List<Order> addOrders(@RequestBody List<Order> orders){
        return service.saveOrders(orders);
    }

    @GetMapping(path = "/orderById/{id}", produces = MediaType.APPLICATION_XML_VALUE)
    public Order findOrderById(@PathVariable int id) throws ResourceNotFoundException{
        return service.getOrderById(id);
    }

    @GetMapping(path = "/order/{username}", produces = MediaType.APPLICATION_XML_VALUE)
    public List<Order> findOrderByUsername(@PathVariable String username){
        return service.getOrdersByUsername(username);
    }

    @GetMapping(path = "/orders", produces = MediaType.APPLICATION_XML_VALUE)
    public List<Order> findAllOrders(){
        return service.getOrders();
    }

    @PutMapping(
            path = "/update",
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
            consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public Order findOrderById(@RequestBody Order order) throws ResourceNotFoundException {
        return service.updateOrder(order);
    }

    @DeleteMapping(path = "/delete/{id}")
    public String deleteOrder(@PathVariable int id) throws ResourceNotFoundException {
        return service.deleteOrder(id);
    }
}
