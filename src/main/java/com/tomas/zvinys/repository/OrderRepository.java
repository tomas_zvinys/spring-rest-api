/* Can be called as DAO layer (Data Access Object pattern) */

package com.tomas.zvinys.repository;

import com.tomas.zvinys.models.entities.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface OrderRepository extends JpaRepository<Order, Integer> {
    Optional<List<Order>> findByUsername(String username);
}
