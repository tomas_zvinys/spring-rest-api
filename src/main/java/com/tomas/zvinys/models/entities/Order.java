package com.tomas.zvinys.models.entities;

import com.tomas.zvinys.models.UnconfirmedOrder;
import lombok.*;

import javax.persistence.*;

/** Order Entity (Model) **/
//@Data   // Generate public getters and public void setters for all attributes
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "order_tbl")
//@Builder
public class Order extends UnconfirmedOrder{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter //Only generate getter for this attribute
    private int id;

    @Getter
    @Setter
    @Column(name = "username", unique = false)   //Just an example how to specify column properties
    private String username;

    @Getter
    @Setter
    @Column
    private int priority;

    @Getter
    @Setter
    @Column // Attribute name matches DB column name by default
    private double discount;

    public Order(UnconfirmedOrder unconfirmedOrder) {
        this.username = unconfirmedOrder.getUsername();
        this.priority = unconfirmedOrder.getPriority();
        this.discount = unconfirmedOrder.getDiscount();
    }

    public static Order confirm(UnconfirmedOrder unconfirmedOrder){
        Order order = new Order(unconfirmedOrder);
        order.setOrderStatus(ORDER_CONFIRMED);
        return order;
    }

    @Override
    public String toString() {
        return "Order(id=" + this.getId() + ", username=" + this.getUsername() + ", priority=" + this.getPriority() +
                ", discount=" + this.getDiscount() + ", orderStatus=" + this.getOrderStatus() + ")";
    }

    // ToDo: persist item list once it's implemented
    // @Column
    // List<Item> itemList;
}
