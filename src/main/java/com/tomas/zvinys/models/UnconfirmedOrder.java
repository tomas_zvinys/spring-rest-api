package com.tomas.zvinys.models;

import com.tomas.zvinys.SpringRestApiApplication;
import com.tomas.zvinys.models.entities.Order;
import lombok.*;

import java.util.UUID;

@Data
@ToString
public class UnconfirmedOrder {
    private String username;
    private int priority;
    private double discount;
    private int orderStatus;

    public static final int ORDER_RECEIVED = 1;
    public static final int ORDER_QUEUED = 2;
    public static final int ORDER_CONFIRMED = 3;

    public void setPriority(int priority) {
        // Limit values to range [0 .. 10] for RabbitMQ priority Queue
        this.priority = Integer.min(Integer.max(priority, 0), SpringRestApiApplication.MAX_PRIORITY);
    }

    // ToDo: implement item list to the unconfirmedOrders
    // List<Item> itemList;
}
