package com.tomas.zvinys.service;

import com.tomas.zvinys.models.UnconfirmedOrder;
import com.tomas.zvinys.models.entities.Order;
import com.tomas.zvinys.exceptions.ResourceNotFoundException;
import com.tomas.zvinys.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;


    @Transactional
    public Order confirmOrder(UnconfirmedOrder unconfirmedOrder){
        // Danger - if the auto-generated id is duplicate - it would make things crash (loop):
        // - attempt to save duplicate id to MYSQL would cause an error
        // - the error would prevent message retrieval from queue and thus queue would never get empty
        Order confirmedOrder = Order.confirm(unconfirmedOrder);
        return orderRepository.save(confirmedOrder);
    }

    /** Create (POST) methods **/

    @Transactional
    public Order saveOrder(Order order){
        return orderRepository.save(order);
    }

    @Transactional
    public List<Order> saveOrders(List<Order> orders){
        return orderRepository.saveAll(orders);
    }

    /** Retrieve (GET) methods **/

    @Transactional
    public Order getOrderById(int id){
        return orderRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("User with id = " + id + "not found"));
    }

    @Transactional
    public List<Order> getOrdersByUsername(String username){
        return orderRepository.findByUsername(username).orElseThrow(() -> new ResourceNotFoundException("User with name = " + username + " has no orders"));
    }

    @Transactional
    public List<Order> getOrders(){
        return orderRepository.findAll();
    }

    /** Update (PUT) methods **/

    @Transactional
    public Order updateOrder(Order order){
        Order storedOrder = orderRepository.findById(order.getId())
                .orElseThrow(() -> new ResourceNotFoundException("User with id = " + order.getId() + "not found"));
        storedOrder.setUsername(order.getUsername());
        storedOrder.setPriority(order.getPriority());
        storedOrder.setDiscount(order.getDiscount());
        return orderRepository.save(storedOrder);
    }

    /** Delete methods **/

    @Transactional
    public String deleteOrder(int id) throws ResourceNotFoundException {
        Order storedOrder = orderRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("User with id = " + id + "not found"));
        orderRepository.deleteById(storedOrder.getId());
        return "Order " + id + " Deleted";
    }
}
