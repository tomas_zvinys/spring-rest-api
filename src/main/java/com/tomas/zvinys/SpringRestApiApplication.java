package com.tomas.zvinys;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
public class SpringRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringRestApiApplication.class, args);
	}

	// ToDo: Store these constants elsewhere (file / DB / whatever), so changing them would not require changing the code
	public static final String EXCHANGE = "my_exchange";
	public static final String ROUTING_KEY = "my_routingKey";
	public static final String MSG_QUEUE = "my_msgQueue";
	public static final int MAX_PRIORITY = 3;


	public static final String REPLY_MSG_QUEUE = "amq.rabbitmq.reply-to";
	public static final String REPLY_ROUTING_KEY = "my_replyRoutingKey";

	// Declare Exchange (AKA message broker) - it distributes messages over queue(s)
	@Bean
	public DirectExchange exchange(){
		return new DirectExchange(EXCHANGE);
	}

	// Declare Binding - it binds Exchange to a message Queue using routing key
	@Bean
	public Binding binding(Queue queue, DirectExchange exchange){
		return BindingBuilder.bind(queue).to(exchange).with(ROUTING_KEY);
	}

	// Declare message queue - it temporarily stores requests until a Consumer(s) (=Receiver(s)) consume them
	@Bean
	public Queue queue(){
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("x-max-priority", MAX_PRIORITY);

		return new Queue(MSG_QUEUE, true, false, false, args);
	}

	@Bean
	public Queue replyQueue(){
		return new Queue(REPLY_MSG_QUEUE, true, false, false);
	}

	// Message converter (to interpret messages as JSON objects instead of strings)
	@Bean
	public MessageConverter jsonMessageConverter(){
		return new Jackson2JsonMessageConverter();
	}

	/** Spring Boot automatically creates a connection factory and a RabbitTemplate,
	 * reducing the amount of code you have to write. **/

	// Template publishes publishes message to queue (?)
	@Bean
	public AmqpTemplate template(ConnectionFactory connectionFactory){
		final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
		rabbitTemplate.setMessageConverter(jsonMessageConverter());
		return rabbitTemplate;
	}
}