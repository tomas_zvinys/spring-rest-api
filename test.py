import requests
from concurrent.futures import ThreadPoolExecutor as PoolExecutor


def testApi(payload):
    url = "http://localhost:8081/queueApi/addOrder"

    headers = {
        'Content-Type': "application/xml",
        # 'Host': "httpbin.org",
        # 'content-length': "42",
        'Connection': "keep-alive",
        }
    if payload != "":
        print(payload)
        response = requests.request("POST", url, data=payload, headers=headers)
        print(response.text)


priority = 0
username = 0
orders = []
for i in range(100):
    if priority < 3:
        priority += 1
    else:
        priority = 0

    if username < 5:
        username += 1
    else:
        username = 0

    usernameAttr = "\t<username>\"{}\"</username>\n".format("username" + str(username))
    priorityAttr = "\t<priority>{}</priority>\n".format(priority)
    discountAttr = "\t<discount>{}</discount>\n".format(1)
    order = "<order>\n" + usernameAttr + priorityAttr + discountAttr + "</order>"
    # print(order)
    orders.append(order)


with PoolExecutor(max_workers=10) as executor:
    for order in executor.map(testApi, orders):
        # print(order)
        pass

